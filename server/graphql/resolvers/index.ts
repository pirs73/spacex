import { IResolvers } from 'apollo-server-express';
import { launchesQueris } from './Launches';
import { launchpadsQueries } from './Launchpads';
import { rocketsQueries } from './Rockets';

export const resolvers: IResolvers = {
  Query: {
    ...launchesQueris,
    ...launchpadsQueries,
    ...rocketsQueries,
  },
};
