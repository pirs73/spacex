import { useQuery } from '@apollo/client';
import { GET_ROCKETS } from '../queries';

export const useGetRockets = () => useQuery(GET_ROCKETS);
