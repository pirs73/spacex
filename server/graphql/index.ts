import { ApolloServer } from 'apollo-server-express';
import { resolvers } from './resolvers';
import { typeDefs } from './typeDefs';

const createApolloServer = () => {
  const apolloServer = new ApolloServer({ typeDefs, resolvers });
  return apolloServer;
};

export { createApolloServer };
