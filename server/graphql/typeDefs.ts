import { gql } from 'apollo-server-express';
import { launchesTypes, launchpadsTypes, rocketsTypes } from './types';

export const typeDefs = gql`
  ${launchesTypes}
  ${launchpadsTypes}
  ${rocketsTypes}

  type Query {
    launches: [Launch]
    filtredLaunches(idL: ID, idR: ID): [Launch]

    launchpads: [Launchpad]
    rockets: [Rocket]
  }
`;
