import { OptionData } from '../../interfaces';
import { lauhcnpadIDVar, rocketIDVar } from '../../cache';

interface FormSelectProps {
  data: OptionData[];
  type: string;
}

const FormSelect = ({ data, type }: FormSelectProps) => {
  const handleChange = (value: string) => {
    if (type === 'launchpad') {
      lauhcnpadIDVar(value);
    }

    if (type === 'rocket') {
      rocketIDVar(value);
    }
  };
  return (
    <select
      className="form-select"
      onChange={(e) => handleChange(e.target.value)}
    >
      <option defaultValue=""></option>
      {data.length > 0 &&
        data.map((item: OptionData) => (
          <option key={item.id} value={item.id}>
            {item.name}
          </option>
        ))}
    </select>
  );
};

FormSelect.getServerSideProps = async (props: FormSelectProps) => {
  return { props };
};

export { FormSelect };
