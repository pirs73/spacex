import { IRockets } from '../../../../interfaces';

export const rocketsQueries = {
  rockets: async (_root: undefined, _args: undefined): Promise<IRockets> => {
    try {
      const res = await fetch('https://api.spacexdata.com/v4/rockets');

      if (!res) {
        throw new Error('failed to find rockets');
      }

      const data = await res.json();

      return data;
    } catch (error) {
      throw new Error(error.message);
    }
  },
};
