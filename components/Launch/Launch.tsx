import { ILaunch } from '../../interfaces';

import styles from './Launch.module.css';

interface LaunchProps {
  launch: ILaunch;
}

const Launch = ({ launch }: LaunchProps) => {
  const formatDate = (dateLocal: string) => {
    const date = new Date(dateLocal);
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    return `${day}.${month}.${year}`;
  };
  return (
    <>
      <div className="col-2">
        <img
          src={launch.links.patch.small}
          alt={launch.name}
          className={styles.launch__image}
        />
      </div>
      <div className="col-10">
        <div className={styles.launch__header}>
          <h4>{launch.name}</h4>
          <p className={styles.launch__date}>{formatDate(launch.date_local)}</p>
        </div>
        <div>
          <p className={styles.launch__details}>{launch.details}</p>
        </div>
      </div>
    </>
  );
};

export { Launch };
