import { InMemoryCache, makeVar } from '@apollo/client';
import { ILaunch } from '../interfaces';

export const launchesVar = makeVar<ILaunch[]>([]);
export const lauhcnpadIDVar = makeVar<string>('');
export const rocketIDVar = makeVar<string>('');

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        launchesData: {
          read() {
            return launchesVar();
          },
        },
        lauhcnpadID: {
          read() {
            return lauhcnpadIDVar();
          },
        },
        rocketID: {
          read() {
            return rocketIDVar();
          },
        },
      },
    },
  },
});
