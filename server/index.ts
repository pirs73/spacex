/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-var-requires */
require('dotenv').config();
import express from 'express';
import next from 'next';
import { createApolloServer } from './graphql';

const port = parseInt(process.env.PORT || '3000', 10);
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  const apolloServer = createApolloServer();
  apolloServer.applyMiddleware({ app: server, path: '/api/v1' });

  server.all('*', (req: any, res: any) => {
    return handle(req, res);
  });

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});
