export interface IRocket {
  id: string;
  name: string;
}

export interface IRockets {
  rockets: IRocket[];
}
