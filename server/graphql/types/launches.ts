export const launchesTypes = `
  type Patch {
    small: String
    large: String
  }

  type Links {
    patch: Patch
  }

  type Launch {
    id: ID
    name: String
    links: Links
    date_local: String
    details: String
    launchpad: String
    rocket: String
  }
`;
