export interface ILaunchpad {
  id: string;
  name: string;
}

export interface ILaunchpads {
  launchpads: ILaunchpad[];
}
