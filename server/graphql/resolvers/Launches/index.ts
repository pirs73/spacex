import { ILaunch, FilterArgs } from '../../../../interfaces';

const getLaunches = async () => {
  try {
    const res = await fetch('https://api.spacexdata.com/v4/launches');

    if (!res) {
      throw new Error('failed to find launches');
    }

    const data = await res.json();

    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const launchesQueris = {
  launches: async (_root: undefined, _args: undefined): Promise<ILaunch[]> => {
    return await getLaunches();
  },

  filtredLaunches: async (
    _root: undefined,
    { idL, idR }: FilterArgs,
  ): Promise<ILaunch[]> => {
    const data = await getLaunches();

    if (idL !== '' && idR !== '') {
      const filtredLaunch = await data.filter(
        (launch: ILaunch) => launch.launchpad === idL,
      );

      return await filtredLaunch.filter(
        (launch: ILaunch) => launch.rocket === idR,
      );
    }

    if (idL !== '' && idR === '') {
      const filtredLaunch = await data.filter(
        (launch: ILaunch) => launch.launchpad === idL,
      );

      return filtredLaunch;
    }

    if (idL === '' && idR !== '') {
      const filtredLaunch = await data.filter(
        (launch: ILaunch) => launch.rocket === idR,
      );

      return filtredLaunch;
    }

    return data;
  },
};
