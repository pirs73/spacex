import { gql } from '@apollo/client';

const launchResponse = `
  id
  name
  links {
    patch {
      small
    }
  }
  date_local
  details
  launchpad
  rocket
`;

export const GET_FILTRED_LAUNCHES = gql`
  query FiltredLaunches($idL: ID, $idR: ID) {
    filtredLaunches(idL: $idL, idR: $idR) {
      ${launchResponse}
    }
  }
`;
