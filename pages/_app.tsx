import { ReactNode } from 'react';
import type { AppProps } from 'next/app';
import '../assets/css/styles.css';

interface SafeHydrateProps {
  children?: ReactNode;
}

function SafeHydrate({ children }: SafeHydrateProps) {
  return (
    <div suppressHydrationWarning>
      {typeof window === 'undefined' ? null : children}
    </div>
  );
}

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <SafeHydrate>
      <Component {...pageProps} />
    </SafeHydrate>
  );
};

export default MyApp;
