import { ILaunchpads } from '../../../../interfaces';

export const launchpadsQueries = {
  launchpads: async (
    _root: undefined,
    _args: undefined,
  ): Promise<ILaunchpads> => {
    try {
      const res = await fetch('https://api.spacexdata.com/v4/launchpads');

      if (!res) {
        throw new Error('failed to find launchpads');
      }

      const data = await res.json();

      return data;
    } catch (error) {
      throw new Error(error.message);
    }
  },
};
