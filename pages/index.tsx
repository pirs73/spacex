import { getDataFromTree } from '@apollo/client/react/ssr';
import { useReactiveVar } from '@apollo/client';
import { lauhcnpadIDVar, rocketIDVar } from '../cache';
import withApollo from '../hoc/withApollo';
import {
  useGetLaunchpads,
  useGetRockets,
  useGetFiltredLaunches,
} from '../apollo/actions';
import { ILaunch } from '../interfaces';
import { FormSelect, Launch } from '../components';

const useInitialData = (idL: string, idR: string) => {
  const { data: dataLP } = useGetLaunchpads();
  const launchpads = (dataLP && dataLP.launchpads) || [];

  const { data: dataR } = useGetRockets();
  const rockets = (dataR && dataR.rockets) || [];

  const {
    loading: loadindFL,
    error: errorFL,
    data,
  } = useGetFiltredLaunches({
    variables: { idL, idR },
  });
  const filtredLaunches = (data && data.filtredLaunches) || [];

  return {
    launchpads,
    rockets,
    loadindFL,
    errorFL,
    filtredLaunches,
  };
};

const Home = () => {
  const lauhcnpadID = useReactiveVar(lauhcnpadIDVar);
  const rocketID = useReactiveVar(rocketIDVar);
  const { launchpads, rockets, loadindFL, errorFL, filtredLaunches } =
    useInitialData(lauhcnpadID, rocketID);

  if (errorFL) {
    return (
      <div>
        <p>error</p>
      </div>
    );
  }

  return (
    <div className="container">
      <h2>Launches</h2>
      <div className="row mt-4">
        <div className="col-5">
          <p className="mb-3">Launch Site</p>
          <FormSelect data={launchpads} type="launchpad" />
        </div>
        <div className="col-5">
          <p className="mb-3">Rockets</p>
          <FormSelect data={rockets} type="rocket" />
        </div>
      </div>
      {loadindFL && (
        <div className="row no-gutters mt-4">
          <div className="col-12">
            <p>Loading...</p>
          </div>
        </div>
      )}
      {!loadindFL &&
        filtredLaunches.length > 0 &&
        filtredLaunches.map((launch: ILaunch) => (
          <div key={launch.id} className="row no-gutters mt-4">
            <Launch launch={launch} />
          </div>
        ))}
      {!loadindFL && filtredLaunches.length === 0 && (
        <div className="row no-gutters mt-4">
          <div className="col-12">
            <p>Not found</p>
          </div>
        </div>
      )}
    </div>
  );
};

export default withApollo(Home, { getDataFromTree });
