import { QueryHookOptions, useQuery } from '@apollo/client';
import { /* GET_LAUNCHES, */ GET_FILTRED_LAUNCHES } from '../queries';

// export const useGetLaunches = () => useQuery(GET_LAUNCHES);

export const useGetFiltredLaunches = (options: QueryHookOptions) =>
  useQuery(GET_FILTRED_LAUNCHES, options);
