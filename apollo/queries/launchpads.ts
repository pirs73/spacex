import { gql } from '@apollo/client';

export const GET_LAUNCHPADS = gql`
  query Launchpads {
    launchpads {
      id
      name
    }
  }
`;
