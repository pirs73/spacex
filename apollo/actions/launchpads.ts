import { useQuery } from '@apollo/client';
import { GET_LAUNCHPADS } from '../queries';

export const useGetLaunchpads = () => useQuery(GET_LAUNCHPADS);
