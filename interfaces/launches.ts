interface IPatch {
  small: string;
}

interface ILinks {
  patch: IPatch;
}

export interface ILaunch {
  id: string;
  name: string;
  links: ILinks;
  date_local: string;
  details: string;
  launchpad: string;
  rocket: string;
}

export interface FilterArgs {
  idL: string;
  idR: string;
}

export interface OptionData {
  id: string;
  name: string;
}
